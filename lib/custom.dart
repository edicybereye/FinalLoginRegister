import 'package:flutter/material.dart';

class FormInput extends StatelessWidget {
  FormInput(
      {this.labeltext,
      this.valueResponse,
      this.obscureValue,
      this.inputType,
      this.valueSave});

  final String labeltext, valueResponse;
  final FormFieldSetter valueSave;
  final bool obscureValue;
  final TextInputType inputType;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: TextFormField(
        obscureText: obscureValue,
        decoration: InputDecoration(
            labelText: labeltext,
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
        validator: (value) {
          if (value.isEmpty) {
            return valueResponse;
          }
        },
        keyboardType: inputType,
        onSaved: valueSave,
      ),
    );
  }
}

class TextView20Size extends StatelessWidget {
  final String data;
  final VoidCallback functionNew;
  TextView20Size({this.data, this.functionNew});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: FlatButton(
        onPressed: functionNew ,
        child: Text(
          data,
          style: TextStyle(fontSize: 16.0),
        ),
      ),
    );
  }
}
