import 'package:flutter/material.dart';
import 'auth.dart';

class HomePage extends StatelessWidget {
  HomePage({this.auth, this.onSignedOut});
  final BaseAuth auth;
  final VoidCallback onSignedOut;

  void signOut() async {
    try {
      await auth.signOut();
      onSignedOut();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HomePage'),
        actions: <Widget>[
          IconButton(
            onPressed: signOut,
            icon: Icon(Icons.lock_open),
          )
        ],
      ),
      body: Container(
        child: Center(
          child: Text(
                "Welcome",
                style: TextStyle(fontSize: 20.0),
              ),
        ),
      ),
    );
  }
}
