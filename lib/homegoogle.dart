import 'package:auth_firebase/login_page.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'custom.dart';

class HomeGoogle extends StatefulWidget {
  HomeGoogle({this.user, this.googleSignIn});
  final FirebaseUser user;
  final GoogleSignIn googleSignIn;
  @override
  _HomeGoogleState createState() => _HomeGoogleState();
}

class _HomeGoogleState extends State<HomeGoogle> {
  void signOut() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        height: 250.0,
        child: Column(
          children: <Widget>[
            ClipOval(
              child: Image.network(widget.user.photoUrl),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                "Sign Out??",
                style: TextStyle(fontSize: 16.0),
              ),
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    widget.googleSignIn.signOut();
                    Navigator.of(context).pushReplacement(new MaterialPageRoute(
                        builder: (BuildContext context) => new LoginPage()));
                  },
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.check),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                      ),
                      Text("Yes")
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.check),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                      ),
                      Text("No")
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
    showDialog(context: context, child: alertDialog);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Google Sign In"),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              setState(() {
                signOut();
              });
            },
            icon: Icon(Icons.lock_open),
          )
        ],
      ),
      body: Container(
        child: Center(
          child: Text(widget.user.displayName),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(new MaterialPageRoute(
              builder: (BuildContext context) => AddClass()));
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class AddClass extends StatefulWidget {
  @override
  _AddClassState createState() => _AddClassState();
}

class _AddClassState extends State<AddClass> {
  String name = '';
  String address = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form Add Item"),
      ),
      body: Form(
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              FormInput(
                inputType: TextInputType.text,
                labeltext: "Full Name",
                obscureValue: false,
                valueResponse: "Please fill full name",
               
              ),
               FormInput(
                inputType: TextInputType.text,
                labeltext: "Address",
                obscureValue: false,
                valueResponse: "Please fill address",
               
              )
            ],
          ),
        ),
      ),
    );
  }
}
