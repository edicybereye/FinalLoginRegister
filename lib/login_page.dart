import 'dart:async';

import 'package:auth_firebase/homegoogle.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'custom.dart';
import 'auth.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.auth, this.onSignIn});
  final BaseAuth auth;
  final VoidCallback onSignIn;
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = new GlobalKey<FormState>();
  String email;
  String password;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  bool validateSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void validateAndSubmit() async {
    if (validateSave()) {
      try {
        String userId =
            await widget.auth.signInWithEmailandPassword(email, password);
        widget.onSignIn();
        print('Login Users : $userId');
      } catch (e) {
        print('Error : $e');
      }
    }
  }

  Future<FirebaseUser> signInGoogle() async {
    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    GoogleSignInAuthentication authentication =
        await googleSignInAccount.authentication;

    FirebaseUser user = await _firebaseAuth.signInWithGoogle(
        idToken: authentication.idToken,
        accessToken: authentication.accessToken);

        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context)=>new HomeGoogle(user: user,googleSignIn:  googleSignIn,)
        ));
        
  }

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
    }

  @override
    void dispose() {
      // TODO: implement dispose
      super.dispose();
    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Login Auth"),
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              FormInput(
                valueSave: (value) => email = value,
                labeltext: "Email Address",
                valueResponse: "Please Fill Email Address",
                inputType: TextInputType.emailAddress,
                obscureValue: false,
              ),
              FormInput(
                valueSave: (value) => password = value,
                labeltext: "Password",
                valueResponse: "Please Fill Password",
                inputType: TextInputType.text,
                obscureValue: true,
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: RaisedButton(
                  onPressed: validateAndSubmit,
                  color: Colors.blue,
                  child: Text(
                    "Sign In",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              TextView20Size(
                data: "Create a new account",
                functionNew: () {
                  Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => new RegisterPage(
                              auth: Auth(),
                            ),
                      ));
                },
              ),
              RaisedButton(
                onPressed: (){
                  signInGoogle();
                },
                color: Colors.blue,
                child: Text(
                  "Google Sign In",
                  style: TextStyle(color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class RegisterPage extends StatefulWidget {
  final BaseAuth auth;
  RegisterPage({this.auth});
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String email;
  String password;
  final _formKey = new GlobalKey<FormState>();
  bool validateSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void validateAndSubmit() async {
    if (validateSave()) {
      try {
        String userId =
            await widget.auth.createUserWithEmailAndPassword(email, password);
        print("Registered Users : $userId");
      } catch (e) {
        print('Error : $e');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form Registration"),
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              FormInput(
                valueSave: (value) => email = value,
                labeltext: "Email Address",
                valueResponse: "Please Fill Email Address",
                inputType: TextInputType.emailAddress,
                obscureValue: false,
              ),
              FormInput(
                valueSave: (value) => password = value,
                labeltext: "Password",
                valueResponse: "Please Fill Password",
                inputType: TextInputType.text,
                obscureValue: true,
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: RaisedButton(
                  onPressed: validateAndSubmit,
                  color: Colors.blue,
                  child: Text(
                    "Sign Up",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              TextView20Size(
                data: "Already have account, click in here",
                functionNew: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
