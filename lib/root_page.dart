import 'package:flutter/material.dart';
import 'login_page.dart';
import 'auth.dart';
import 'home_page.dart';

class RootPage extends StatefulWidget {
  final BaseAuth auth;
  RootPage({this.auth});
  @override
  _RootPageState createState() => _RootPageState();
}

enum AuthStatus { notSignIn, signIn }

class _RootPageState extends State<RootPage> {
  AuthStatus authStatus = AuthStatus.notSignIn;

  initState() {
    super.initState();
    widget.auth.checkUsers().then((userId) {
      setState(() {
        authStatus = userId == null ? AuthStatus.notSignIn : AuthStatus.signIn;
      });
    });
  }

  void _signedIn(){
    setState(() {
          authStatus = AuthStatus.signIn;
        });
  }

   void _signOut(){
    setState(() {
          authStatus = AuthStatus.notSignIn;
        });
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.notSignIn:
        return new LoginPage(
          auth: widget.auth,
          onSignIn: _signedIn,
        );
      case AuthStatus.signIn:
        return new HomePage(
          auth: widget.auth,
          onSignedOut: _signOut,
         
        //   appBar: AppBar(
        //     title: Text("Home Page"),
        //     actions: <Widget>[
        //       Icon(Icons.lock_open),
        //       Padding(padding: EdgeInsets.all(10.0),)
        //     ],
        //   ),
          
        //   body: Container(
        //     child: Text("Welcome"),
        //   ),
        );
    }
  }
}
